<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Elevate
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<meta name="application-name" content="<?php bloginfo( 'name' ); ?>"/>

	<!-- Chrome for Android -->
	<meta name="theme-color" content="#ba0c2f" />

	<!-- iOS icons -->
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-152x152.png" />

	<!-- Favicon -->
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-192.png" sizes="192x192" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-128.png" sizes="128x128" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<span class="site-overlay"></span>
	<div class="page-wrapper">
		<a class="sr-only skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'elevate' ); ?></a>

		<?php get_template_part( 'template-parts/uga', 'header' ); ?>
		<header class="site-header">
			<div class="container site-nav">
				<div class="row align-items-center">
					<div class="col-3 col-md-2 col-lg-1">
						<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<picture>
								<!--[if IE 9]><video style="display: none;"><![endif]-->
									<?php
									if ( get_theme_mod( 'elevate_desktop_logo' ) ) :
										?>
										<source srcset="<?php echo get_theme_mod( 'elevate_desktop_logo' ); ?>" media="(min-width: 992px)">
									<?php else : ?>
										<source srcset="<?php echo get_template_directory_uri(); ?>/img/elevate-georgia-logo-square.png" media="(min-width: 992px)">
									<?php endif; ?>
									<?php
									if ( get_theme_mod( 'elevate_tablet_logo' ) ) :
										?>
										<source srcset="<?php echo get_theme_mod( 'elevate_tablet_logo' ); ?>" media="(min-width: 576px)">
									<?php else : ?>
										<source srcset="<?php echo get_template_directory_uri(); ?>/img/elevate-georgia-logo-horizontal-2018.png" media="(min-width: 576px)">
									<?php endif; ?>
									
								<!--[if IE 9]></video><![endif]-->
								<?php
								if ( get_theme_mod( 'elevate_mobile_logo' ) ) :
									?>
									<img srcset="<?php echo get_theme_mod( 'elevate_mobile_logo' ); ?>" alt="<?php esc_attr( bloginfo( 'name' ) ); ?>" />
								<?php else : ?>
									<img srcset="<?php echo get_template_directory_uri(); ?>/img/elevate-georgia-logo-square.png" alt="<?php esc_attr( bloginfo( 'name' ) ); ?>" />
								<?php endif; ?>
							</picture>
						</a>
					</div>
					<div class="menubar d-none d-lg-flex col-9 col-md-10 col-lg-11">
						<nav id="site-navigation" class="d-none d-lg-flex">
							
							<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'primary-menu',
								'menu_class'	 => 'desktop-nav'
							) );
							?>
						</nav><!-- #site-navigation -->
					</div>
					<div class="col d-block d-lg-none text-right">
						<a class="btn btn-secondary" href="#offcanvas" title="<?php esc_attr_e( 'Primary Menu', 'elevate' ); ?>"><i class="fa fa-bars" aria-hidden="true"></i></a>
					</div>
					<!-- <div class="hidden-md-down col-lg-2 text-right">
						<a href="http://www.uga.edu/">
							<img src="<?php echo get_template_directory_uri(); ?>/img/GEORGIA-FS-FC-sm.png" alt="University of Georgia" />
						</a>
					</div> -->
				</div>
			</div>
		</header>
		<main>
