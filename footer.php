<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Elevate
 */

?>

	</main>

	<?php if ( has_nav_menu( 'menu-2' ) ) : ?>
	<footer class="site-footer">
		<div class="container">
			<div class="footer-heading"><span>Resources</span></div>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-2',
				'menu_id'        => 'footer-menu',
				'menu_class'	 => '',
				'container' => 'nav',
				'container_id' => 'footer-navigation',
				'container_class' => 'row align-items-start justify-content-between d-flex flex-xl-nowrap',
				'walker' => new Elevate_Footer_Walker(),
				'items_wrap' => '%3$s'
			) );
			?>
		</div>
	</footer>
	<?php endif; ?>

	<?php get_template_part( 'template-parts/uga', 'footer' ); ?>
</div><!-- /.page -->
<nav id="offcanvas">
	<div>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" style="display: block;">
			<img srcset="<?php echo get_theme_mod( 'elevate_desktop_logo' ); ?>" alt="<?php esc_attr( bloginfo( 'name' ) ); ?>" style="max-width: 200px; margin: 0 auto;" />
		</a>					
		<?php
		wp_nav_menu( array(
			'theme_location' => 'menu-1',
			'menu_id'        => 'primary-menu',
			'container' => '',
		) );
		?>
	</div>
</nav><!-- #site-navigation -->

<?php wp_footer(); ?>

</body>
</html>
