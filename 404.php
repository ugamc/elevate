<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Elevate
 */

get_header();
?>

	<section class="error-404 not-found">
		<header class="page-header">
			<div class="container">
				<h1 class="page-title"><?php esc_html_e( 'Page Not Found.', 'elevate' ); ?></h1>
			</div>
		</header><!-- .page-header -->

		<div class="page-content">
			<div class="container">
				<p><?php esc_html_e( 'Sorry, nothing’s at this address.', 'elevate' ); ?></p>
			</div>
		</div><!-- .page-content -->
	</section><!-- .error-404 -->

<?php
get_footer();
