<?php
/**
 * Elevate Theme Customizer
 *
 * @package Elevate
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function elevate_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'elevate_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'elevate_customize_partial_blogdescription',
		) );
	}
	$wp_customize->add_section('elevate_header', array(
		'title' => 'Header',
		'description' => '',
		'priority' => 120,
	));
	// add a setting for the site logo
	$wp_customize->add_setting('elevate_mobile_logo');
	// Add a control to upload the logo
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'elevate_mobile_logo', array(
		'label' => 'Mobile Logo',
		'section' => 'elevate_header',
		'settings' => 'elevate_mobile_logo',
	) ) );
	// add a setting for the site logo
	$wp_customize->add_setting('elevate_tablet_logo');
	// Add a control to upload the logo
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'elevate_tablet_logo', array(
		'label' => 'Tablet Logo',
		'section' => 'elevate_header',
		'settings' => 'elevate_tablet_logo',
	) ) );
	// add a setting for the site logo
	$wp_customize->add_setting('elevate_desktop_logo');
	// Add a control to upload the logo
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'elevate_desktop_logo', array(
		'label' => 'Desktop Logo',
		'section' => 'elevate_header',
		'settings' => 'elevate_desktop_logo',
	) ) );
}
add_action( 'customize_register', 'elevate_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function elevate_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function elevate_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function elevate_customize_preview_js() {
	wp_enqueue_script( 'elevate-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), false, true );
}
add_action( 'customize_preview_init', 'elevate_customize_preview_js' );
