<?php
/**
 * Elevate functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Elevate
 */

if ( ! function_exists( 'elevate_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function elevate_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Elevate, use a find and replace
		 * to change 'elevate' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'elevate', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'elevate' ),
			'menu-2' => esc_html__( 'Footer', 'elevate' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'elevate_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'elevate_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function elevate_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'elevate_content_width', 640 );
}
add_action( 'after_setup_theme', 'elevate_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function elevate_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'elevate' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'elevate' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'elevate_widgets_init' );

class Elevate_Footer_Walker extends Walker_Nav_Menu {
    
	// Displays start of an element. E.g '<li> Item Name'
    // @see Walker::start_el()
    function start_el(&$output, $item, $depth=0, $args=array(), $id = 0) {
    	$object = $item->object;
    	$type = $item->type;
    	$title = $item->title;
    	$description = $item->description;
    	$permalink = $item->url;

    	array_push($item->classes, "col-6", "col-md-6", "col-xl-auto", "mb-1", "mb-xl-0");

    	$classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

    	$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

    	$output .= '<div' . $class_names . '">';
        
    	//Add SPAN if no Permalink
    	if( $permalink && $permalink != '#' ) {
      		$output .= '<a href="' . $permalink . '">';
    	} else {
    		$output .= '<span>';
    	}
       
    	$output .= $title;

    	if( $permalink && $permalink != '#' ) {
      		$output .= '</a>';
    	} else {
      		$output .= '</span>';
    	}

    	$output .= '</div>';
    }
}

/* 
 * Remove search function
 */
function elevate_disable_search( $query, $error = true ) {
	if ( is_search() && !is_admin() ) {
		$query->is_search = false;
		// $query->query_vars[s] = false;
		// $query->query[s] = false;
		if ( $error == true )
			$query->is_404 = true;
	}
}
add_action( 'parse_query', 'elevate_disable_search' );

add_filter( 'get_search_form', function(){ return null; } );

function remove_search_widget() {
    unregister_widget('WP_Widget_Search');
}
add_action( 'widgets_init', 'remove_search_widget' );

/**
 * Enqueue scripts and styles.
 */
function elevate_scripts() {
	wp_enqueue_style( 'elevate-style', get_stylesheet_uri() );

	wp_enqueue_script( 'elevate-navigation', get_template_directory_uri() . '/js/navigation.js', array(), false, true );
	wp_enqueue_script( 'elevate-app', get_template_directory_uri() . '/js/app.js', array(), false, true );

	wp_enqueue_script( 'elevate-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), false, true );

	/* UGA Footer */
	wp_enqueue_style( 'uga-footer', get_template_directory_uri() . '/css/uga-footer.css', array('elevate-style'), false );

	/* UGA Header */
	wp_enqueue_style( 'uga-header', get_template_directory_uri() . '/css/uga-header.css', array('elevate-style'), false );
	wp_enqueue_script( 'uga-header', get_template_directory_uri() . '/js/uga-header.js', array('jquery'), false, true );

	wp_enqueue_script( 'fontawesome', 'https://use.fontawesome.com/releases/v5.7.2/js/fontawesome.js', array(), '5.7.2', true );
	wp_enqueue_script( 'fontawesome-solid', 'https://use.fontawesome.com/releases/v5.7.2/js/solid.js', array('fontawesome'), '5.7.2', true );
	wp_enqueue_script( 'fontawesome-brands', 'https://use.fontawesome.com/releases/v5.7.2/js/brands.js', array('fontawesome'), '5.7.2', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'elevate_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

