<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elevate
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<!-- <header class="page-header">
		<div class="container">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</header> -->
	
	<?php
	$thumbnail = get_the_post_thumbnail_url($post->ID, 'large' );
	printf('<div
		class="homepage-hero py-5"
		style="
			background: linear-gradient(
	    		rgba(0, 0, 0, 0.7), 
	    		rgba(0, 0, 0, 0.7)
	    	), transparent url(\'%s\') center center no-repeat;
	    	background-size: cover;
	    	color: #fff;
	    	text-shadow: 0 1px .5px rgba(0,0,0,.8);
    	">', $thumbnail );
	?>
		<div class="container">
			<div class="row align-items-center">
				<div class="col-sm-6 col-md-3 mb-5 mb-sm-0">
					<?php printf('<img src="%s" alt="">', get_template_directory_uri() . '/img/georgia-on-my-mind-white.svg'); ?>
				</div>
				<div class="col-sm-6 col-md-9 col-lg-8 offset-lg-1">
					<?php printf( '<h1 class="display-4">%s</h1><div class="lead">June 9–11, 2019</div>', get_bloginfo() ); ?>
				</div>
			</div>
		</div>
	</div>

	<div id="content" class="page-content">
		<div class="container">
			<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elevate' ),
				'after'  => '</div>',
			) );
			?>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
