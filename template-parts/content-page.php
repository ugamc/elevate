<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elevate
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		$backgroundImg = '';
		$backgroundStyle = '';

		if ( has_post_thumbnail() ) {
			$backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
			$backgroundStyle = sprintf( ' style="background-image: url(%s);"', $backgroundImg[0] );
		}
		printf( '<header class="page-header%s"%s>', $backgroundImg ? ' has-background' : '', $backgroundStyle ? $backgroundStyle : '' );
	?>
	
		<div class="container">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</header>

	<div id="content" class="page-content">
		<div class="container">
			<?php
			the_content();
			
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'elevate' ),
				'after'  => '</div>',
			) );
			?>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
