<?php
/**
 * Template part for displaying UGA header
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Elevate
 */

?>

<header class="ugaheader ugaheader--black ~ugaheader--border ~ugaheader--border-red">
	<div class="ugaheader__container ugaheader__container--max-widths">
		<div class="ugaheader__row">
			<div class="ugaheader__wordmark ugaheader__wordmark--shield">
				<a class="ugaheader__wordmark-link" href="https://www.uga.edu/">
					University of Georgia
				</a>
			</div>
			<div class="ugaheader__nav-container">
				<!-- Menu toggle button displays on smaller screen sizes -->
				<button id="ugaheader__nav-menu-toggle" class="ugaheader__nav-menu-toggle" aria-expanded="false" aria-label="Toggle Menu">
					<i class="fas fa-fw fa-caret-down" title="Toggle Menu" aria-hidden></i>
				</button>
				
				<nav class="ugaheader__nav">
					<!-- Standard Links -->
					<ul class="ugaheader__nav-list ugaheader__nav-links">
						<li class="ugaheader__nav-list-element">
							<a class="ugaheader__nav-list-link" href="https://mc.uga.edu/">Division of Marketing &amp; Communications</a>
						</li>
					</ul>
					<!-- Search Element -->
					<form action="https://www.uga.edu/search.php" class="ugaheader__search">
						<button type="button" id="ugaheader__search-toggle" class="ugaheader__search-toggle" aria-expanded="false" aria-label="Toggle Search">
							<i class="fas fa-fw fa-search" title="Toggle Search" aria-hidden></i>
						</button>
						<div id="ugaheader__search-field" class="ugaheader__search-field">
							<label id="ugaheader__search-label" class="ugaheader__form-label" for="ugaheader__search-input">Search</label>
							<input id="ugaheader__search-input" class="ugaheader__search-input" type="search" name="q" placeholder="Search UGA sites" aria-labelledby="ugaheader__search-label" autocomplete="off">
						</div>
						<button type="submit" class="ugaheader__search-submit">Submit</button>
					</form>
				</nav>
			</div>
		</div>
	</div>
</header>